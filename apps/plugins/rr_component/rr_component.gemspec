$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rr_component/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rr_component"
  s.version     = RrComponent::VERSION
  s.authors     = ["Hugo Bento"]
  s.email       = ["tostasqb@gmail.com"]
  s.homepage    = ""
  s.summary     = ": Summary of RrComponent."
  s.description = ": Description of RrComponent."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.3"

  s.add_development_dependency "sqlite3"
end
