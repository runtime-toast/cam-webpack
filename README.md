# Camaleon + Rails + React + Webpack

## Preparation & install

Dev environment with:

- Rails 5.1
- Ruby 2.4.1
- Node
- Yarn
- Webpack
- webpacker gem through `gem install webpacker`

__1: create rails project__

One command to create a rails project with webpack and react...but this is not the end of it.

        rails new cam-webpack --javascript=JAVASCRIPT --webpack=react

__1.2: or on an existing project__

- Add `gem 'webpacker'` to the Gemfile
- Run `./bin/rails webpacker:install:react`

You should have an `app/javascript/packs` directory if everything is ok.

---

__2. add camaleon to project__

- add `gem 'camaleon_cms'` and
- `gem 'draper', '~> 3'` in Gemfile
- Run
        bundle install
        rails generate camaleon_cms:install
        rake db:migrate

__3. add the rails-react gem__

This step is optional, you can still use react without this gem but it's a facilitator for you to be able to render react on the server side and using a helper on the rails view.

- add 'gem `react-rails`' to your Gemfile and bundle
- `rails generate react:install` to setup

## A nice to have, setup of dev environment

- create `Procfile` on root folder with content

        web: bundle exec puma -p $PORT 

- create `Procfile.dev` on root folder with content

        web: bundle exec rails s
        # watcher: ./bin/webpack-watcher
        webpacker: ./bin/webpack-dev-server

- create a file called `server` inside `bin` folder with content

        #!/bin/bash -i
        bundle install
        bundle exec foreman start -f Procfile.dev

- add gem 'foreman' to the development group of the _Gemfile_
- `chmod 777 bin/server` to change permissions
- Finally run `bin/server` to start the server

## Some examples

__a) Example counter module (not react)__

- Add a file `index.js` to a `app/javascript/counter` folder with the content

        // A simple counter example
        // The setup will be more complicated in modern apps built using React

        const incrementNode = document.getElementById('increment');
        const decrementNode = document.getElementById('decrement');
        const inputNode = document.getElementById('counter');

        const counter = {
          initialize() {
            incrementNode.addEventListener('click', (event) => {
              event.preventDefault();
              const currentValue = inputNode.value;
              inputNode.value = parseInt(currentValue, 0) + 1;
            });

            decrementNode.addEventListener('click', (event) => {
              event.preventDefault();
              const currentValue = inputNode.value;
              if (currentValue > 0) {
                inputNode.value = parseInt(currentValue, 0) - 1;
              }
            });
          }
        };

        export default counter;

- Add file `style.sass` in the module folder with some sass styles of choice

        $grey: #f2f2f2

        .counter-wrapper
          max-width: 500px
          margin: 100px auto
          padding: 10px
          border: 1px solid $grey

          form
            margin-bottom: 10px

        #counter
        #increment,
        #decrement
          padding: 5px 10px
          margin-bottom: 10px
          display: inline-block

- Add `counter.html.erb` to the same folder

        <div class=”counter-wrapper”>
            <h1>Counter Module</h1>
            <form class="counter">
                <button id="increment">Increment</button>
                <input id="counter" type=”number” name=”counter” value="0" />
                <button id="decrement">Decrement</button>
            </form>
        </div>

- Add `<%= javascript_pack_tag 'counter' %>` to a view to call the counter module
- Add `<%= stylesheet_pack_tag 'counter' %>` to the view

__b) Example react component (without react-rails)__

- Add `hello_react.jsx` to the folder `app/javascript/packs`

        import React from 'react'
        import ReactDOM from 'react-dom'
        import PropTypes from 'prop-types'

        const Hello = props => (
          <div>This is {props.name}!</div>
        )

        Hello.defaultProps = {
          name: 'David'
        }

        Hello.propTypes = {
          name: PropTypes.string
        }

        document.addEventListener('DOMContentLoaded', () => {
          ReactDOM.render(
            <Hello name="React" />,
            // document.body.appendChild(document.createElement('div')),
            document.body.insertBefore(document.createElement('span'), document.body.childNodes[0]),
          )
        });

- Add `<%= javascript_pack_tag 'hello_react' %>` in your view to use component 
- Or `import '../hello_react'` to the `application.js` of the `packs` folder

__c) Example react component (with react-rails)__

- Add folder `hello` inside the `components` folder

        import React from 'react'
        import ReactDOM from 'react-dom'

        class Hello extends React.Component {
          constructor(props) {
            super(props);
            this.state = {date: new Date()};
          }

          render() {
            return (
              <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
              </div>
            );
          }
        }

        export default Hello;

- In any view use `<%=react_component("hello") %>`

__d) add your react component through a shortcode__

- Create a partial in `app/apps/themes/theme-name/views/partial` called `_my_shortcode_component.html.erb` with content
- Inside the partial add `<%=react_component("hello") %>`
- You should have a file called `custom_helper.rb` inside the root theme's folder, add this method

        def my_helper_before_app_load
          shortcode_add('my_shortcode_component', theme_view('partials/_my_shortcode_component'))
          # other shortcuts go here 
        end

- Add this snippet to the `hooks` node of `config/config.json` of your theme, 

        "app_before_load": [
          "my_helper_before_app_load"
        ]
      
- Use the shorcode `[my_shortcode_component]` in the WYSIWYG of any post and see your component rendered

## Additional config options (keep in mind)

I. Check `config/webpacker.yml`. This file controls where assets should be placed and where they will be compiled to

- source_path: frontend
- source_entry_path: packs
- public_output_path: assets/packs # outputs to => public/assets/packs

II. There is an option for adding existing assets from the project (like app/assets) to webpack. Refer to the documentation [here](https://github.com/rails/webpacker#resolved-paths)

---

### Notes for production

- config.serve_static_files = true
- `secret_key_base` on `config/secrets.yml`
- RAILS_ENV=production rake assets:precompile

### References

1. https://medium.com/statuscode/introducing-webpacker-7136d66cddfb  
2. https://github.com/rails/webpacker  
3. http://camaleon.tuzitio.com/documentation/category/40757-developer-docs/installation-1.html